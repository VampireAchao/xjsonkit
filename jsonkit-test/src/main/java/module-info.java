module jsonkit.gson {
    requires jsonkit.core;
    requires data.core;
    requires junit;
    exports com.ejlchina.json.test;
}