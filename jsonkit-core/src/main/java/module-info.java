module jsonkit.core {
    uses com.ejlchina.json.JSONFactory;
    requires data.core;
    exports com.ejlchina.json;
}